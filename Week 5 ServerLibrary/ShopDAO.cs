﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week_5_ServerLibrary
{
    class ShopDAO
    {
        public bool LoginUser(string username, string password)
        {
            using (ShopModelContainer ctx = new ShopModelContainer())
            {
                var customer = (from a in ctx.Customers
                                    where a.Username.Equals(username) && a.Password.Equals(password)
                                    select a);

                if (customer.Any())
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public string RegisterUser(string username, string password)
        {
            using (ShopModelContainer ctx = new ShopModelContainer())
            {
                var user = from a in ctx.Customers
                where a.Username.Equals(username)
                select a;

                if (user.Any())
                {
                    // deze user bestaat al
                    return "Deze user betaat al bedenk een originelere username";
                }

                // create a new customer
                Customer newCustomer = new Customer();
                newCustomer.Username = username;
                newCustomer.Password = password;
                newCustomer.Balance = 25.00;

                // add customer to context
                ctx.Customers.Add(newCustomer);

                // save context
                ctx.SaveChanges();
                return "true";
            }

        }

        public double GetUserBalans(string username)
        {
            using (ShopModelContainer ctx = new ShopModelContainer())
            {
                return (from a in ctx.Customers
                where a.Username.Equals(username)
                select a.Balance).Sum();
            }
        }

        public List<String> GetUserInventory(string username)
        {
            using (ShopModelContainer ctx = new ShopModelContainer())
            {
                var products = (from a in ctx.Customers
                                where a.Username.Equals(username)
                                select a.CustomerProducts1).ToList();

                List<String> inventoryList = new List<String>();

                foreach (var p in products)
                {
                    List<CustomerProduct1> productList = p.ToList();
                    foreach (CustomerProduct1 product in productList)
                    {
                        inventoryList.Add(product.Product.Name + ", amount: " + product.Amount);
                    }
                }
                return inventoryList;
            }
        }

        public List<String> GetAvailableProduct()
        {
            using (ShopModelContainer ctx = new ShopModelContainer())
            {
                List<Product> products = (from a in ctx.Products
                                          where a.Amount > 0
                               select a).ToList<Product>();

                List<String> productsList = new List<string>();

                foreach (Product product in products)
                {
                    productsList.Add(product.Name + ": $" + product.Price);
                }
                return productsList;
            }
        }

        public string BuyProduct(List<String> products, string username)
        {
            using (ShopModelContainer ctx = new ShopModelContainer())
            {
                double userBalance = this.GetUserBalans(username);

                List<Product> productss = (from a in ctx.Products
                                           where a.Amount > 0
                                          select a).ToList<Product>();

                List<Product> checkedProducts = new List<Product>();
                double totalPrice = 0;

                foreach (string product in products)
                {
                    string productt = product.Split(':')[0];

                    var x = (from a in ctx.Products
                             where a.Name.Equals(productt) && a.Amount > 0
                             select a);


                    if(!x.Any())
                    {
                        return "Dit product is niet meer beschikbaar!";
                    } else
                    {
                        Product y = (from a in ctx.Products
                                     where a.Name.Equals(productt) && a.Amount > 0
                                     select a).First();
                        checkedProducts.Add(y);
                        totalPrice = totalPrice + y.Price;
                    }
                }
                
                if(userBalance > totalPrice)
                {
                    // user can buy the products
                    foreach (Product checkedProduct in checkedProducts)
                    {
                        var query = (from a in ctx.Customers
                                     where a.Username.Equals(username)
                                     select a).First();

                        bool doif = true;

                        // Execute the query, and change the column values
                        // you want to change.
                        foreach (CustomerProduct1 customer in query.CustomerProducts1)
                        {
                            if (customer.Product.Name.Equals(checkedProduct.Name))
                            {

                                var a = customer.Amount;
                                customer.Amount = a + 1;
                                doif = false;
                            }
                        } 
                        
                        if(doif) {  

                            CustomerProduct1 customerproduct = new CustomerProduct1();
                            customerproduct.Product = checkedProduct;
                            customerproduct.Customer = query;
                            customerproduct.Amount = 1;

                            query.CustomerProducts1.Add(customerproduct);
                        }

                        var balance = query.Balance;
                        query.Balance = balance - checkedProduct.Price;

                        var product = (from a in ctx.Products
                                       where a.Name.Equals(checkedProduct.Name)
                                       select a).First();

                        var amount = product.Amount;
                        product.Amount = amount - 1;

                        ctx.SaveChanges();
                    }
                    return "true";
                } else
                {
                    return "Not enough money to buy the products";
                }
            }
        }
    }
}
