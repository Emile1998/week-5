﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Week_5_ServerLibrary
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IShopService" in both code and config file together.
    [ServiceContract]
    public interface IShopService
    {
        [OperationContract]
        bool LoginUser(string username, string password);

        [OperationContract]
        string RegisterUser(string username);

        [OperationContract]
        double GetUserMoneyBalans(string username);
    
        [OperationContract]
        List<string> GetInventory(string username);

        [OperationContract]
        List<string> GetProducts();

        [OperationContract]
        string BuyProduct(List<string> products, string username);
    }
}
