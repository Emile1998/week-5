﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Week_5_ServerLibrary
{
    
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "ShopService" in both code and config file together.
    public class ShopService : IShopService
    {
        ShopDAO shopDAO = new ShopDAO();
  
        public bool LoginUser(string username, string password)
        {
            return shopDAO.LoginUser(username, password);
        }

        public string RegisterUser(string username)
        {
            // generate password by reversing username
            string newPassword =  new string(Enumerable.Range(1, username.Length).Select(i => username[username.Length - i]).ToArray());

            // request dao to add user
            return this.shopDAO.RegisterUser(username, newPassword);
        }

        public double GetUserMoneyBalans(string username)
        {
            // request the dao to return the current balance from user
            return this.shopDAO.GetUserBalans(username);
        }
        public List<string> GetInventory(string username)
        {
            // request the dao to return the inventory of the user
            return this.shopDAO.GetUserInventory(username);
        }

        public List<string> GetProducts()
        {
            return this.shopDAO.GetAvailableProduct();
        }
        public string BuyProduct(List<string> products, string username)
        {
            return this.shopDAO.BuyProduct(products, username);
        }
    }
}
