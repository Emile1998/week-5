﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Week_5_GUI
{
    public partial class LoginRegisterForm : Form
    {
        private ShopEngine shopEngine;

        public LoginRegisterForm()
        {
            this.shopEngine = new ShopEngine();
            InitializeComponent();
        }

        private void loginButton_Click(object sender, EventArgs e)
        {
            if (this.shopEngine.LoginUser(this.loginUsernameInput.Text, this.loginPasswordInput.Text))
            {
                // close current window and open the shop form
                this.Hide();
                ShopForm shopForm = new ShopForm(this.loginUsernameInput.Text);
                shopForm.Closed += (s, args) => this.Close();
                shopForm.Show();
            } else
            {
                // The username and password are not working, show an error on the screen
                this.errorLabel.Text = "Customer not registered!";
            }
        }

        private void registerBurron_Click(object sender, EventArgs e)
        {
            var a = this.shopEngine.RegisterUser(this.registerUsernameInput.Text);
            if (!a.Equals("true"))
            {
                this.registerPasswordLabel.Text = a;
            }
            else
            {
                this.tabView.SelectTab(0);
            }
        }

        private void updatePasswordLabel(object sender, EventArgs e)
        {
            this.registerPasswordLabel.Text = "Password: " + new string(Enumerable.Range(1, this.registerUsernameInput.Text.Length).Select(i => this.registerUsernameInput.Text[this.registerUsernameInput.Text.Length - i]).ToArray()); ;
        }
    }
}
