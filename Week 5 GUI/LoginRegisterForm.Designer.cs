﻿namespace Week_5_GUI
{
    partial class LoginRegisterForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabView = new System.Windows.Forms.TabControl();
            this.loginTab = new System.Windows.Forms.TabPage();
            this.loginButton = new System.Windows.Forms.Button();
            this.loginPasswordInput = new System.Windows.Forms.TextBox();
            this.loginPasswordLabel = new System.Windows.Forms.Label();
            this.errorLabel = new System.Windows.Forms.Label();
            this.loginUsernameLabel = new System.Windows.Forms.Label();
            this.loginUsernameInput = new System.Windows.Forms.TextBox();
            this.registerTab = new System.Windows.Forms.TabPage();
            this.registerButton = new System.Windows.Forms.Button();
            this.registerPasswordLabel = new System.Windows.Forms.Label();
            this.registerUsernameLabel = new System.Windows.Forms.Label();
            this.registerUsernameInput = new System.Windows.Forms.TextBox();
            this.tabView.SuspendLayout();
            this.loginTab.SuspendLayout();
            this.registerTab.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabView
            // 
            this.tabView.Controls.Add(this.loginTab);
            this.tabView.Controls.Add(this.registerTab);
            this.tabView.Location = new System.Drawing.Point(0, -2);
            this.tabView.Name = "tabView";
            this.tabView.SelectedIndex = 0;
            this.tabView.Size = new System.Drawing.Size(308, 181);
            this.tabView.TabIndex = 0;
            // 
            // loginTab
            // 
            this.loginTab.Controls.Add(this.loginButton);
            this.loginTab.Controls.Add(this.loginPasswordInput);
            this.loginTab.Controls.Add(this.loginPasswordLabel);
            this.loginTab.Controls.Add(this.errorLabel);
            this.loginTab.Controls.Add(this.loginUsernameLabel);
            this.loginTab.Controls.Add(this.loginUsernameInput);
            this.loginTab.Location = new System.Drawing.Point(4, 22);
            this.loginTab.Name = "loginTab";
            this.loginTab.Padding = new System.Windows.Forms.Padding(3);
            this.loginTab.Size = new System.Drawing.Size(300, 155);
            this.loginTab.TabIndex = 0;
            this.loginTab.Text = "Login";
            this.loginTab.UseVisualStyleBackColor = true;
            // 
            // loginButton
            // 
            this.loginButton.Location = new System.Drawing.Point(178, 114);
            this.loginButton.Name = "loginButton";
            this.loginButton.Size = new System.Drawing.Size(75, 23);
            this.loginButton.TabIndex = 5;
            this.loginButton.Text = "Login";
            this.loginButton.UseVisualStyleBackColor = true;
            this.loginButton.Click += new System.EventHandler(this.loginButton_Click);
            // 
            // loginPasswordInput
            // 
            this.loginPasswordInput.Location = new System.Drawing.Point(153, 51);
            this.loginPasswordInput.Name = "loginPasswordInput";
            this.loginPasswordInput.PasswordChar = '*';
            this.loginPasswordInput.Size = new System.Drawing.Size(100, 20);
            this.loginPasswordInput.TabIndex = 4;
            // 
            // loginPasswordLabel
            // 
            this.loginPasswordLabel.AutoSize = true;
            this.loginPasswordLabel.Location = new System.Drawing.Point(21, 54);
            this.loginPasswordLabel.Name = "loginPasswordLabel";
            this.loginPasswordLabel.Size = new System.Drawing.Size(53, 13);
            this.loginPasswordLabel.TabIndex = 3;
            this.loginPasswordLabel.Text = "Password";
            // 
            // errorLabel
            // 
            this.errorLabel.AutoSize = true;
            this.errorLabel.ForeColor = System.Drawing.Color.Red;
            this.errorLabel.Location = new System.Drawing.Point(134, 89);
            this.errorLabel.Name = "errorLabel";
            this.errorLabel.Size = new System.Drawing.Size(0, 13);
            this.errorLabel.TabIndex = 2;
            // 
            // loginUsernameLabel
            // 
            this.loginUsernameLabel.AutoSize = true;
            this.loginUsernameLabel.Location = new System.Drawing.Point(21, 22);
            this.loginUsernameLabel.Name = "loginUsernameLabel";
            this.loginUsernameLabel.Size = new System.Drawing.Size(55, 13);
            this.loginUsernameLabel.TabIndex = 1;
            this.loginUsernameLabel.Text = "Username";
            // 
            // loginUsernameInput
            // 
            this.loginUsernameInput.Location = new System.Drawing.Point(153, 22);
            this.loginUsernameInput.Name = "loginUsernameInput";
            this.loginUsernameInput.Size = new System.Drawing.Size(100, 20);
            this.loginUsernameInput.TabIndex = 0;
            // 
            // registerTab
            // 
            this.registerTab.Controls.Add(this.registerButton);
            this.registerTab.Controls.Add(this.registerPasswordLabel);
            this.registerTab.Controls.Add(this.registerUsernameLabel);
            this.registerTab.Controls.Add(this.registerUsernameInput);
            this.registerTab.Location = new System.Drawing.Point(4, 22);
            this.registerTab.Name = "registerTab";
            this.registerTab.Padding = new System.Windows.Forms.Padding(3);
            this.registerTab.Size = new System.Drawing.Size(300, 155);
            this.registerTab.TabIndex = 1;
            this.registerTab.Text = "Register";
            this.registerTab.UseVisualStyleBackColor = true;
            // 
            // registerButton
            // 
            this.registerButton.Location = new System.Drawing.Point(172, 109);
            this.registerButton.Name = "registerButton";
            this.registerButton.Size = new System.Drawing.Size(75, 23);
            this.registerButton.TabIndex = 3;
            this.registerButton.Text = "Register";
            this.registerButton.UseVisualStyleBackColor = true;
            this.registerButton.Click += new System.EventHandler(this.registerBurron_Click);
            // 
            // registerPasswordLabel
            // 
            this.registerPasswordLabel.AutoSize = true;
            this.registerPasswordLabel.Location = new System.Drawing.Point(106, 72);
            this.registerPasswordLabel.Name = "registerPasswordLabel";
            this.registerPasswordLabel.Size = new System.Drawing.Size(0, 13);
            this.registerPasswordLabel.TabIndex = 2;
            // 
            // registerUsernameLabel
            // 
            this.registerUsernameLabel.AutoSize = true;
            this.registerUsernameLabel.Location = new System.Drawing.Point(22, 24);
            this.registerUsernameLabel.Name = "registerUsernameLabel";
            this.registerUsernameLabel.Size = new System.Drawing.Size(55, 13);
            this.registerUsernameLabel.TabIndex = 1;
            this.registerUsernameLabel.Text = "Username";
            // 
            // registerUsernameInput
            // 
            this.registerUsernameInput.Location = new System.Drawing.Point(109, 21);
            this.registerUsernameInput.Name = "registerUsernameInput";
            this.registerUsernameInput.Size = new System.Drawing.Size(138, 20);
            this.registerUsernameInput.TabIndex = 0;
            this.registerUsernameInput.TextChanged += new System.EventHandler(this.updatePasswordLabel);
            // 
            // LoginRegisterForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(305, 178);
            this.Controls.Add(this.tabView);
            this.Name = "LoginRegisterForm";
            this.Text = "Form1";
            this.tabView.ResumeLayout(false);
            this.loginTab.ResumeLayout(false);
            this.loginTab.PerformLayout();
            this.registerTab.ResumeLayout(false);
            this.registerTab.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabView;
        private System.Windows.Forms.TabPage loginTab;
        private System.Windows.Forms.TabPage registerTab;
        private System.Windows.Forms.TextBox loginUsernameInput;
        private System.Windows.Forms.Button loginButton;
        private System.Windows.Forms.TextBox loginPasswordInput;
        private System.Windows.Forms.Label loginPasswordLabel;
        private System.Windows.Forms.Label errorLabel;
        private System.Windows.Forms.Label loginUsernameLabel;
        private System.Windows.Forms.Label registerUsernameLabel;
        private System.Windows.Forms.TextBox registerUsernameInput;
        private System.Windows.Forms.Label registerPasswordLabel;
        private System.Windows.Forms.Button registerButton;
    }
}

