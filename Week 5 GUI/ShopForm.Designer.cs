﻿namespace Week_5_GUI
{
    partial class ShopForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.productsList = new System.Windows.Forms.CheckedListBox();
            this.productsLabel = new System.Windows.Forms.Label();
            this.inventoryLabel = new System.Windows.Forms.Label();
            this.inventoryList = new System.Windows.Forms.ListView();
            this.currentMoneyLabel = new System.Windows.Forms.Label();
            this.buyButton = new System.Windows.Forms.Button();
            this.refreshButton = new System.Windows.Forms.Button();
            this.shopErrorLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // productsList
            // 
            this.productsList.FormattingEnabled = true;
            this.productsList.Location = new System.Drawing.Point(306, 49);
            this.productsList.Name = "productsList";
            this.productsList.Size = new System.Drawing.Size(216, 244);
            this.productsList.TabIndex = 0;
            // 
            // productsLabel
            // 
            this.productsLabel.AutoSize = true;
            this.productsLabel.Location = new System.Drawing.Point(303, 19);
            this.productsLabel.Name = "productsLabel";
            this.productsLabel.Size = new System.Drawing.Size(49, 13);
            this.productsLabel.TabIndex = 1;
            this.productsLabel.Text = "Products";
            // 
            // inventoryLabel
            // 
            this.inventoryLabel.AutoSize = true;
            this.inventoryLabel.Location = new System.Drawing.Point(34, 19);
            this.inventoryLabel.Name = "inventoryLabel";
            this.inventoryLabel.Size = new System.Drawing.Size(51, 13);
            this.inventoryLabel.TabIndex = 3;
            this.inventoryLabel.Text = "Inventory";
            // 
            // inventoryList
            // 
            this.inventoryList.Location = new System.Drawing.Point(37, 49);
            this.inventoryList.Name = "inventoryList";
            this.inventoryList.Size = new System.Drawing.Size(216, 244);
            this.inventoryList.TabIndex = 4;
            this.inventoryList.UseCompatibleStateImageBehavior = false;
            // 
            // currentMoneyLabel
            // 
            this.currentMoneyLabel.AutoSize = true;
            this.currentMoneyLabel.Location = new System.Drawing.Point(34, 309);
            this.currentMoneyLabel.Name = "currentMoneyLabel";
            this.currentMoneyLabel.Size = new System.Drawing.Size(100, 13);
            this.currentMoneyLabel.TabIndex = 5;
            this.currentMoneyLabel.Text = "Current money label";
            // 
            // buyButton
            // 
            this.buyButton.Location = new System.Drawing.Point(350, 326);
            this.buyButton.Name = "buyButton";
            this.buyButton.Size = new System.Drawing.Size(75, 23);
            this.buyButton.TabIndex = 6;
            this.buyButton.Text = "Buy";
            this.buyButton.UseVisualStyleBackColor = true;
            this.buyButton.Click += new System.EventHandler(this.buyButton_Click);
            // 
            // refreshButton
            // 
            this.refreshButton.Location = new System.Drawing.Point(447, 326);
            this.refreshButton.Name = "refreshButton";
            this.refreshButton.Size = new System.Drawing.Size(75, 23);
            this.refreshButton.TabIndex = 7;
            this.refreshButton.Text = "Refresh";
            this.refreshButton.UseVisualStyleBackColor = true;
            this.refreshButton.Click += new System.EventHandler(this.refreshButton_Click);
            // 
            // shopErrorLabel
            // 
            this.shopErrorLabel.AutoSize = true;
            this.shopErrorLabel.ForeColor = System.Drawing.Color.Red;
            this.shopErrorLabel.Location = new System.Drawing.Point(303, 310);
            this.shopErrorLabel.Name = "shopErrorLabel";
            this.shopErrorLabel.Size = new System.Drawing.Size(0, 13);
            this.shopErrorLabel.TabIndex = 8;
            // 
            // ShopForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(579, 383);
            this.Controls.Add(this.shopErrorLabel);
            this.Controls.Add(this.refreshButton);
            this.Controls.Add(this.buyButton);
            this.Controls.Add(this.currentMoneyLabel);
            this.Controls.Add(this.inventoryList);
            this.Controls.Add(this.inventoryLabel);
            this.Controls.Add(this.productsLabel);
            this.Controls.Add(this.productsList);
            this.Name = "ShopForm";
            this.Text = "Form2";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckedListBox productsList;
        private System.Windows.Forms.Label productsLabel;
        private System.Windows.Forms.Label inventoryLabel;
        private System.Windows.Forms.ListView inventoryList;
        private System.Windows.Forms.Label currentMoneyLabel;
        private System.Windows.Forms.Button buyButton;
        private System.Windows.Forms.Button refreshButton;
        private System.Windows.Forms.Label shopErrorLabel;
    }
}