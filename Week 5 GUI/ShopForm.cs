﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Week_5_GUI
{
    public partial class ShopForm : Form
    {
        private ShopEngine shopEngine;
        private string currentUsername;
        public ShopForm(string username)
        {
            this.shopEngine = new ShopEngine();
            this.currentUsername = username;

            InitializeComponent();
            // we have to fill some labels before doing anything
            this.updateGUI();

        }

        private void refreshButton_Click(object sender, EventArgs e)
        {
            this.updateGUI();
        }

        private void buyButton_Click(object sender, EventArgs e)
        {
            List<String> list = this.productsList.CheckedItems.Cast<string>().ToList();
            if (list.Count() > 0)
            {
                string error = this.shopEngine.BuyProduct(list, this.currentUsername);

                if (!error.Equals("true"))
                {
                    this.shopErrorLabel.Text = error;
                }
            }

            this.updateGUI();
        }

        private void updateGUI()
        {
            List<String> inventory = this.shopEngine.GetInventory(this.currentUsername);
            List<String> products = this.shopEngine.GetProducts();
            double money = this.shopEngine.GetUserMoneyBalans(this.currentUsername);

            this.inventoryList.Items.Clear();
            this.productsList.Items.Clear();

            foreach (String product in inventory)
            {
                this.inventoryList.Items.Add(product, 0);
            }

            foreach (String product in products)
            {
                this.productsList.Items.Add(product, 0);
            }
            this.currentMoneyLabel.Text = "Money left: € " + money.ToString();
        }
    }
}
