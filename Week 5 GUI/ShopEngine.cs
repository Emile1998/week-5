﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.CheckedListBox;
using Week_5_GUI.ShopService;

namespace Week_5_GUI
{
    class ShopEngine
    {
        ShopServiceClient shopProxy;

        public ShopEngine()
        {
            shopProxy = new ShopServiceClient();

        }

        // function to log user in 
        public bool LoginUser(string username, string password)
        {
            // logic for checking username server side
            return shopProxy.LoginUser(username, password);
        }

        // function to register user
        public string RegisterUser(string username)
        {
            // we don't want empty users so check that
            if (username != "")
            {
                // request the proxy to register a user with this username
                return shopProxy.RegisterUser(username);
            }
            return "Je moet wel iets invullen";
        }

        // function to get usermoney
        public double GetUserMoneyBalans(string username)
        {
            return shopProxy.GetUserMoneyBalans(username);
        }

        // function to get a list of inventory server side
        public List<String> GetInventory(string username)
        {
            return shopProxy.GetInventory(username).ToList();
        }

        // function to get all available shop products from the server
        public List<String> GetProducts()
        {
            return shopProxy.GetProducts().ToList();
        }

        public string BuyProduct(List<string> products, string username)
        {
            return shopProxy.BuyProduct(products.ToArray(), username);

        }
    }
}
